// ==UserScript==
// @name popupCompanion
// @namespace botsGarden
// @version 1.1.1
// @match *://*/*
// @grant GM_getValue
// @grant GM_addStyle
// @require ./addon.js
// @require ./vue.js
// @require ./modalComponent.js
// ==/UserScript==
// 

// WIP 🚧

function getLocation() {
  let location = {
    hash: window.location.hash,
    host: window.location.host,
    hostname: window.location.hostname,
    href: window.location.href,
    origin: window.location.origin,
    pathname: window.location.pathname,
    port: window.location.port,
    protocol: window.location.protocol
  } 
  console.log(location);
  return location
}

function addItemToNavBar(id) {
  let navbar = document.querySelector("ul.navbar-sub-nav")
  let node = document.createElement("li")
  node.classList.add("d-none")
  node.classList.add("d-lg-block")
  node.classList.add("d-xl-block")
  node.setAttribute("id", id)
  navbar.appendChild(node)
  return node
}

/**
 * Add script values
 * key: hosts
 * values: an array of hosts like that: ["gitlab-faas.eu.ngrok.io", "gitlab.com"] 
 */
function getHosts() {
  return GM_getValue("hosts", ["gitlab.com"]) 
}

(function(){
  let hosts = getHosts()
  let location = getLocation()
  
  if(hosts.includes(location.host)) {

    addItemToNavBar("anchor") // define anchor for the Vue application
    // start app
    new Vue({
      el: '#anchor',
      template:`
        <li>
          <button class="dashboard-shortcuts-web-ide" id="show-modal" @click="showModal = true">👋 Show Modal</button>
          <!-- use the modal component, pass in the prop -->
          <modal v-if="showModal" @close="showModal = false">
              <!--
              you can use custom content here to overwrite
              default content
              <h3 slot="header">custom header</h3>
              -->
          </modal>
        </li>     
      `,
      data: {
       showModal: false
      }
    })
  }
  
})()

