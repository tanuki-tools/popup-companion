# popup-companion

## Requirements

- Install the ViolentMonkey extension
  - from Chrome Store: [https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en)
  - (right now, not tested) from Mozilla add-ons page: [https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)

## Install the "popup-companion" script

- Add script (Click on the **+** button)
- Choose: "Install from url" and add this url: [https://gitlab.com/tanuki-tools/popup-companion/raw/master/index.js](https://gitlab.com/tanuki-tools/popup-companion/raw/master/index.js)
- Once the script loaded, click on "Confirm installation"
- The script is loaded

### Add the GitLab instance domain(s)

To activate the "popup-companion" script on a specific domain of a GitLab instance, you need to add script variables:
- Select the **Values** panel
- Add a script value with:
  - `hosts` as key
  - an array of strings like that `["gitlab-faas.eu.ngrok.io", "gitlab.com"]` as value
- That's all

## Remarks

- When you want to deploy a new version, you need to increment the version number in the header of `index.js` file
