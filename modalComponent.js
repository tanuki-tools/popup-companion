// register modal component

let content = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
`
let title = "👋 Hello World 🌍"

Vue.component('modal', {
  template: `
    <transition name="modal">
        <div class="modal-mask">
        <div class="modal-wrapper">
            <div class="modal-container">

                <div class="modal-header">
                    <slot name="header">
                        <h3>${title}</h3>
                    </slot>
                </div>

                <div class="modal-body">
                    <slot name="body">
                    ${content}
                    </slot>
                </div>

                <div class="modal-footer">
                    <slot name="footer">
                        <button class="btn btn-primary btn-sm btn-block" @click="$emit('close')">OK</button>
                    </slot>
                </div>
            </div>
        </div>
        </div>
    </transition>  
  `
})
